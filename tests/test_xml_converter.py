import requests
import requests_mock
import pytest
import json
from lxml import etree as ET
from xmldiff import main
import base64


from ..app.xml_converter import get_xml, convert_xml, upload_xml


ANNOTATION_ID = "123456"


def match_auth(request):
    """Matcher for checking credentials"""
    credentials = {"username": "myUser123", "password": "secretSecret"}
    return json.loads(request.text) == credentials

def match_little(request):
    """Matcher to check data sended to little endpoint"""
    
    body = json.loads(request.body)

    # checking annotation id
    body["annotationId"] == ANNOTATION_ID

    # checking xml
    good_little = ET.parse('files/little.xml')

    base = body["content"]
    xml = base64.b64decode(base)
    request_little = ET.fromstring(xml)

    diff = main.diff_trees(good_little, request_little)    
    return diff == []



def test_converter(client, app, requests_mock):
    """Test for seamless scenario"""

    # gettin env
    ROSSUM_URL = app.config.get("ROSSUM_URL")
    LITTLE_URL = app.config.get("LITTLE_URL")
    USERNAME = app.config.get("USERNAME")
    PASSWORD = app.config.get("PASSWORD")
    AUTH_TOKEN = "deadbeef"

    # registering uri for mocking 
    requests_mock.register_uri('GET', ROSSUM_URL, 
        json={"annotations": ''.join([ROSSUM_URL,"/annotations"])})

    requests_mock.register_uri('POST', ''.join([ROSSUM_URL,"/auth/login"]), 
        additional_matcher=match_auth, json={"key":AUTH_TOKEN})

    auth_header = {"Authorization" : ''.join(["token ",AUTH_TOKEN])}

    requests_mock.register_uri('GET', 
        ''.join([ROSSUM_URL,"/annotations/",ANNOTATION_ID]),
        headers=auth_header, 
        json={"queue": ''.join([ROSSUM_URL,"/queues/8236"])})

    with open("files/rossum.xml", "rb") as rossum_xml:      
        requests_mock.register_uri('GET', 
            ''.join([ROSSUM_URL,"/queues/8236/export?format=xml"]), 
            headers=auth_header, 
            content=rossum_xml.read())

    requests_mock.register_uri('POST', LITTLE_URL, 
        additional_matcher=match_little, text="success")

    requests_mock.register_uri('GET', 
        "http://127.0.0.1:5000/converter?annotation=123456", 
        real_http=True)

    # checking functionality of app
    success = client.get("http://127.0.0.1:5000/converter?annotation=123456",
        auth=(USERNAME, PASSWORD))

    assert success.status == "200 OK"
    assert json.loads(success.data) == {"success":True}