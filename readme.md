# Application for converting XML

Written as homework assignment for Rossum interview.
Full description of homework could be found in assignment folder.

Example request for converting XML for annotation 123456:

[https://this.application/converter?annotation=123456](https://this.application/converter?annotation=123456)

Username = myUser123
Password = secretSecret

## Note for scaling

This application should be scalable in a way flask application is.
There are no databases, temporarily stored files, locks and similar which usually cause problems.

In assignment, there is a requirement to convert xml data in a scalable way.

If the application is supposed to be prepared for scenario "xml conversion takes really long time",
I would suggest some producer/consumer or broker architecture, for example using celery.
Downside is that these architectures usually sends 201 response while request is being handled.
Final request status is communicated afterwards. This would change behaviour of my application in the way it would be more difficult for others to communicate.

If the application is supposed to be prepared for scenario "more xml annotation needs to be converted on one request", I would suggest using multiprocessing.
But the assignment is quite explicit that annotation is one (even with specified dummy ID).

I decided to write the application in a way it should not be difficult to use similar scalability features, but I stay unsure where to go. For now, flask application is able to deal with decent amount of request in the same time as it is.
