from flask import Flask
from flask_basicauth import BasicAuth
from flask import request
from . import xml_converter as c

def create_app():
    """Create and configure an instance of application."""
    app = Flask(__name__)
    app.config.from_pyfile('config.py')    

    @app.route("/")
    def display_readme():
        """Plain site just to display read me"""        
        with open("readme.md", "r") as f: 
            return ''.join(["<pre>",f.read(),"</pre>"])

    basic_auth = BasicAuth(app)

    @app.route('/converter')
    @basic_auth.required
    def convert():
        """Endpoint for converting"""    
        try:
            annotation_id = request.args.get('annotation')
        except req.exceptions.RequestException:
            print("Network problem while connecting to rossum server")
            return {"success":False}

        result_get, rossum_xml = c.get_xml(annotation_id)
        if result_get == False: return {"success":False}

        result_convert, little_xml = c.convert_xml(rossum_xml, annotation_id)
        if result_convert == False: return {"success":False}

        result_upload = c.upload_xml(little_xml, annotation_id)
        if result_upload == False: return {"success":False}

        return {"success":True}

    return app
