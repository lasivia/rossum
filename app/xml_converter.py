from flask import Flask
from flask import request
from flask import current_app as app
from datetime import date
import base64
import json

import requests as req
from lxml import etree as ET
from dicttoxml import dicttoxml


def get_xml(annotation_id):
    """Connects to rossum server and gets xml for requested annotation

    returns tuple (success, xml)
    """    

    try:
        # getting endpoints
        response = req.get(app.config.get("ROSSUM_URL"))
        addresses = json.loads(response.text)  

        # rossum authentication
        auth_response = req.post(app.config.get("ROSSUM_URL") + "/auth/login",
            data = json.dumps({'username':app.config.get("USERNAME"), 
            'password':app.config.get("PASSWORD")}))
        auth_token = json.loads(auth_response.text)["key"]
        auth_header = {"Authorization" : ''.join(["token ",auth_token])}

        # getting right queue to ask for annotation
        response = req.get(
            ''.join([addresses["annotations"],'/',annotation_id]),
            headers=auth_header)
        queue_address = json.loads(response.text)["queue"]

        # getting xml
        annotation_response = req.get(''.join([queue_address,
            "/export?format=xml"]),
            headers=auth_header)
        rossum_xml = ET.fromstring(annotation_response.content)

    except req.exceptions.RequestException:
        print("Network problem")
        return False, None
    except json.JSONDecodeError:
        print("Response from rossum server couldn't be decoded")
        return False, None
    except (ET.LxmlError, ET.LxmlSyntaxError):
        print("XML from rossum server couldn't be decoded")
        return False, None

    else:
        return True, rossum_xml


def convert_datapoint(datapoint, tags, payable):
    """Convert single datapoint

    params:
    datapoint: node of etree
    tags: dict with conversion table for XML tags
    payable: dict to save converted datapoint value
    """

    item = datapoint.get("schema_id")
    if item in tags:                       
        payable[tags[item]] = datapoint.text


def convert_xml(rossum_xml, annotation_id):
    """Converts rossum xml to little xml

    params:
    rossum_xml: xml from rossum server
    annotation_id: requested annotation id (string)

    returns tuple (success, little_xml)
    """

    # tags conversion table
    tags = {"invoice_id": "InvoiceNumber",
            "date_issue":"InvoiceDate",
            "date_due":"DueDate",
            "iban": "Iban",
            "invoice_description": "Notes",            
            "amount_total": "TotalAmount",
            "amount_total_base": "Amount",
            "sender_name": "Vendor",
            "sender_address": "VendorAddress",
            "item_description": "Notes",
            "item_quantity": "Quantity",
            "item_amount_total": "Amount",
            "item_account_id": "AccountId",
            # tags translating separately thus they need extra processinq:
            "date_issue":"InvoiceDate",
            "date_due":"DueDate",
            "currency": "Currency"}

    # dictionary for annotation extraction
    payable = {"InvoiceNumber": None,
                "InvoiceDate": None, 
                "DueDate": None, 
                "TotalAmount": None, 
                "Notes": None, 
                "Iban": None, 
                "Amount": None,
                "Currency": None,
                "Vendor": None, 
                "VendorAddress": None,
                "Details": None}

    # finding right annotation
    annotation = None
    for a in rossum_xml.iterfind(".//annotation[@url]"):
        if a.attrib['url'].endswith('/' + annotation_id):
            annotation = a
            break
    if annotation is None:
        print("Couldn't find requested annotation in rossum xml")
        return False, None

    # extracting suitable `payable' dict from annotation xml
    content = annotation.find("content")
    if content is not None:
        for section in content.iterfind("section"):
            schema = section.get("schema_id")
            
            if (schema == "invoice_info_section"
                or schema == "amounts_section"
                or schema == "vendor_section"):

                for datapoint in section.iter():
                    # taking care of some values separately
                    item = datapoint.get("schema_id")
                    if item == "date_issue" or item == "date_due":
                        try:                       
                            invoice_date = date.fromisoformat(datapoint.text)
                            payable[tags[item]] = \
                                invoice_date.strftime("%Y-%m-%dT%H:%M:%S")
                        except ValueError:
                            payable[tags[item]] = datapoint.text
                    elif item == "currency":
                        payable[tags[item]] = datapoint.text.upper()
                    # universaly convert everything else
                    else:                        
                        convert_datapoint(datapoint, tags, payable)            
    
            if schema == "line_items_section":
                details = section.find('multivalue')
                details_list = []
                if details is not None:
                    for detail in details.iterfind('tuple'):
                        point = {"Amount":None,"AccountId": None,
                                "Quantity": None, "Notes": None}   
                        for datapoint in detail.iterfind('datapoint'):
                            convert_datapoint(datapoint, tags, point)
                        details_list.append(point)
                payable["Details"] = details_list

    # encapsulate payable into right structure
    little_dict = {"InvoiceRegisters": {"Invoices": {"Payable":payable}}}

    # converting dict to xml
    item_naming_func = lambda x: x[:-1]

    try:
        little_xml = dicttoxml(little_dict, root=False, attr_type=False, 
            item_func=item_naming_func)
    except TypeError:
        print("Internal error while converting XML")
        return False, None

    return True, little_xml


def upload_xml(little_xml, annotation_id):
    """Sends converted XML to little endpoint"""

    # sending xml
    try:
        payload = {"annotationId": annotation_id, 
            "content":base64.b64encode(little_xml).decode("utf-8")}
        response = req.post(app.config.get("LITTLE_URL"),
            data = json.dumps(payload))
    except req.exceptions.RequestException:
        print("Network problem")
        return False

    # checking response
    try:
        response.raise_for_status()
    except requests.exceptions.HTTPError:
        return False

    return True
