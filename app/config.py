from os import environ 
from dotenv import load_dotenv

load_dotenv()

ROSSUM_URL = environ.get("ROSSUM_URL")
LITTLE_URL = environ.get("LITTLE_URL")
USERNAME = environ.get("USERNAME")
PASSWORD = environ.get("PASSWORD")
BASIC_AUTH_USERNAME = environ.get("USERNAME")
BASIC_AUTH_PASSWORD = environ.get("PASSWORD")